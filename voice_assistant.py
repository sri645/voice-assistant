import pyttsx3
import pywhatkit
import wikipedia
from datetime import datetime
import pyjokes
import speech_recognition as sr

def cheppu(answer):
    engine = pyttsx3.init()
    voices = engine.getProperty('voices')
    engine.setProperty('voice', voices[1].id)
    engine.say(answer)
    engine.runAndWait()

def process(question):
    if "what are you doing" in question:
        print("i am waiting for your quetion")
        cheppu("i am waiting for your quetion")
    
    if "name" in question:
        print("my name is alexa")
        cheppu("my name is alexa")

    elif "how are you" in question:
        print("i am good. thank you. how can i help you")
        cheppu("i am good. thank you. how can i help you")
        
    elif "play" in question:
        question = question.replace("play", " ")
        pywhatkit.playonyt(question)
    
    elif "who is" in question:
        question = question.replace("who is", " ")
        print(wikipedia.summary(question, 2))
        cheppu(wikipedia.summary(question, 2))
    
    elif "time" in question:
        time = datetime.today().time().strftime("%I:%M %p")
        print(time)
        cheppu(time)
    
    elif "joke" in question:
        joke = pyjokes.get_joke()
        print(joke)
        cheppu(joke)
    
    elif "love you" in question:
        print("chepputho kodatha")
        cheppu("chepputho kodatha")

    elif "whatsapp" in question:
        phoneno = input("enter phone number")
        msg = input("enter msg")
        hour = int(input("enter hour in 24 hours clock time to send msg"))
        mini = int(input("enter minutes to send msa"))
        pywhatkit.sendwhatmsg(phoneno, msg, hour, mini)


print("say something!!!")
r = sr.Recognizer()
with sr.Microphone() as source:
    audio = r.listen(source)
try:
    question = r.recognize_google(audio)
    print(question)
    if "Alexa" or "alexa" in question:
        question = question.replace("alexa ", " ")
        print(question)
        process(question)
       
except:
    cheppu("Sorry, I can't process your question!")
